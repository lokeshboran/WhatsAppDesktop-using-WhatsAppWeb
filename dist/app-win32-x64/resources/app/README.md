# WhatsApp Desktop App

This project is an application skeleton for [Electron](https://electron.atom.io) desktop app.

It uses bunling systems webpack.

## Getting Started

To get you started you can simply clone the WhatsApp Desktop App repository and install the dependencies:

### Prerequisites

You need git to clone the WhatsApp Desktop App repository. You can get git from
[https://lokeshboran@gitlab.com/lokeshboran/WhatsAppDesktop-using-WhatsAppWeb.git](https://lokeshboran@gitlab.com/lokeshboran/WhatsAppDesktop-using-WhatsAppWeb.git).

We also use a number of node.js tools to initialize and test electron-seed. You must have node.js and
its package manager (npm) installed.  You can get them from [http://nodejs.org/](http://nodejs.org/).

Install these globally:

```
npm install -g electron electron-installer-windows
```

### Clone WhatsApp Desktop App

Clone the WhatsApp Desktop App repository using git:

### Install Dependencies

We have two kinds of dependencies in this project: development tools and application specific packages. They are both managed with npm in package.json as devDependencies and dependencies respectively.

```
npm install
```

## Directory Layout

```
  renderer.js               --> used by renderer process 
  main.js                --> main application module
  index.css                --> main application css file
  index.html            --> app layout file (the main html template file of the app)
```

### Running the App

The WhatsApp Desktop App project comes preconfigured with a local development webserver. It is a webpack-dev-server, that supports hot reload.  You can start this webserver with `npm start`.

```
$ git clone https://lokeshboran@gitlab.com/lokeshboran/WhatsAppDesktop-using-WhatsAppWeb.git
$ cd WhatsAppDesktop-using-WhatsAppWeb
$ npm install
$ npm start
```


Now browse to the app at `http://localhost:8045/`.

### Building the desktop App